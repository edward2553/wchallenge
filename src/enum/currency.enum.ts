export enum CurrencyEnum {
	ARS = 'ARS',
	EUR = 'EUR',
	USD = 'USD'
}