import axios, { AxiosInstance } from 'axios';
import { config } from '../config/config';

let axiosInstance = axios.create({
	baseURL: config.COINGECKO.API_URL,
});

class CoingeckoWSConsumer {

	axiosInstance: AxiosInstance;

	constructor() {
		this.axiosInstance = axios.create({
			baseURL: config.COINGECKO.API_URL,
		});
	}

	async listCoins(vs_currency: string) {
		try {
			const { data } = await this.axiosInstance.get(`/coins/markets`, { params: { vs_currency, order: 'market_cap_desc', sparklike: false, 'per_page': 250 } });
			return data;
		} catch (error) {
			throw error;
		}
	}

	async getCryptocurrenciesByUser(vs_currency: string, ids: string[]) {

		try {
			const { data } = await this.axiosInstance.get('/coins/markets', { params: { vs_currency, ids: ids.toString(), 'order': 'market_cap_desc' } });
			return data;
		} catch (error) {
			throw error;
		}
	}

}

let coingeckoWSConsumer = new CoingeckoWSConsumer();

export default coingeckoWSConsumer;