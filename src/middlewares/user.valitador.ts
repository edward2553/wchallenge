import { NextFunction, Request, Response } from 'express';
import UserSchema from '../database/schemas/users.schema';
import ResponseUtil from '../utils/response.util';


export const userFieldsValidator = async (req: Request, res: Response, next: NextFunction) => {
	let userData = new UserSchema(req.body);

	try {
		await userData.validate(['name', 'lastname', 'username', 'password', 'currency']);
		next();

	} catch (error) {

		res.status(500).json(ResponseUtil.error(error.message.replace('wc_', '')));

	}
}

export const userLoginValidator = async (req: Request, res: Response, next: NextFunction) => {
	let userData = new UserSchema(req.body);

	try {
		await userData.validate(['username', 'password']);
		next();

	} catch (error) {
		//res.status(500).json(ResponseUtil.error(error.message.replace('wc_', '')));
		next(error)

	}
}