import { NextFunction, Request, Response } from 'express';
import CryptoCurrencySchema from '../database/schemas/cryptocurrencies.schema';
import { CurrencyEnum } from '../enum/currency.enum';

export const cryptocurrencyInValidator = async (req: Request, res: Response, next: NextFunction) => {
	let { currency } = req.params;

	if (!currency) {
		next(new Error('El campo de moneda preferida es requerido'));
	}

	if (!Object.keys(CurrencyEnum).includes(currency)) {
		next(new Error('El campo de moneda no es válido'));
	}


	next();

}

export const cryptocurrencyDataValidator = async (req: Request, res: Response, next: NextFunction) => {

	try {
		let { cryptocurrency} = req.body;
		if(!cryptocurrency) throw Error('El campo cryptocurrency es requerido')
		next();
	} catch (error) {
		next(new Error(error.message));

	}

}

export const userCryptocurrenciesValidator = async (req: Request, res: Response, next: NextFunction) => {

	try {
		const username = req.query.username as string;
		const sizeList = req.query.sizeList as string;

		if (!username) {
			throw new Error('El parámetro username es requerido');
		}

		if (sizeList != undefined && (parseInt(sizeList) < 1 || parseInt(sizeList) > 25)) {
			throw new Error('El tamaño de la lista de criptomonedas debe estar entre 1 y 25');
		}

		next();
	} catch (error) {
		next(error);

	}

}