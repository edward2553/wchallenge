import { NextFunction, Request, Response } from 'express';
import { config } from "../config/config";
import logComponent from "../utils/logger.util";

const withErrorStack = (error: string, stack: any) => {
	if (config.dev) {
		return { error, stack }
	}
	return { error };
}

export const logErrors = (error: Error, req: Request, res: Response, next: NextFunction) => {
	logComponent.error(error);
	next(error);
}

export const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction) => {
	return res.status(500).json(withErrorStack(error.message, error.stack))
}