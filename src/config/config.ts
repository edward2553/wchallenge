require('dotenv').config();


export const config = {
  dev: process.env.NODE_ENV?.trim() !== 'production' || false,
  port: process.env.PORT || 4200,
  url: process.env.DB_URL || 'mongodb://localhost:27017/wchallenge',
  JWT: {
    jwt_secret: process.env.JWT_SECRET || 'wchallenge',
    jwt_max_age: process.env.JWT_MAX_AGE || '3d'
  },
  COINGECKO: {
    API_URL: process.env.COINGECKO_API_URL || 'https://api.coingecko.com/api/v3',
    COINGECKO_ATTRS: ['id', 'symbol', 'current_price', 'name', 'image', 'last_updated']
  }
};