import { Document, model, Model, ObjectId } from 'mongoose';
import { CurrencyEnum } from '../enum/currency.enum';
import { ICryptoCurrency } from './cryptocurrency.model';

export interface IUser extends Document {
	_id: ObjectId,
	name: string,
	lastname: string,
	username: string,
	password: string,
	currency: CurrencyEnum,
	cryptocurrencies: string[]
}
export interface IUserModel extends Model<IUser> {
	login(username?: string, password?: string): Promise<IUser>,
	addCryptocurrency(username: string, cryptourrency: string): Promise<IUser>
}

