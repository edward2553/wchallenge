import { Document } from "mongoose";
export interface ICryptoCurrency extends Document {
	id: string,
	symbol: string,
	current_price: number,
	name: string,
	image: string,
	last_updated: Date

}