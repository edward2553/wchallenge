
import { Router } from 'express';
import userController from '../controllers/user.controller';
import { cryptocurrencyDataValidator, userCryptocurrenciesValidator } from '../middlewares/cryptocurrency.valitador';
import { userFieldsValidator, userLoginValidator } from '../middlewares/user.valitador';
import { verifyToken, verifyUser } from '../utils/jwt.util';


class UsersRoutes {

	public userRoutes: Router = Router();

	constructor() {
		this.config();
	}
	config() {
		this.userRoutes.post('/login', userLoginValidator, userController.login);
		this.userRoutes.post('/create', userFieldsValidator, userController.create);
		this.userRoutes.post('/addCryptocurrency', verifyToken, cryptocurrencyDataValidator, verifyUser('body'), userController.addCryptocurrency);
		this.userRoutes.get('/cryptocurrenciesByUser', verifyToken, userCryptocurrenciesValidator, verifyUser('query'), userController.listCryptocurrenciesByUser);
	}

}

const { userRoutes } = new UsersRoutes();

export default userRoutes;