import { Router } from 'express';
import { indexController } from '../controllers/index.controller';
import cryptoCurrenciesRoutes from './crytpocurrencies.routes';
import userRoutes from './users.routes';


class IndexRoutes {
	public IndexRouter: Router = Router();

	constructor() {
		this.config();
	}

	config(): void {
		this.IndexRouter.get('/', indexController.index);
		this.IndexRouter.use('/users', userRoutes);
		this.IndexRouter.use('/cryptoCurrencies', cryptoCurrenciesRoutes);

	}
}


const { IndexRouter } = new IndexRoutes();
export default IndexRouter;