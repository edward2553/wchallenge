
import { Router } from 'express';
import { cryptoCurrenciesController } from '../controllers/cryptocurrencies.controller';
import { cryptocurrencyInValidator } from '../middlewares/cryptocurrency.valitador';


class CryptoCurrenciesRoutes{

	public cryptoCurrenciesRoutes: Router = Router();

	constructor() {
		this.config();
	}
	config() {
		this.cryptoCurrenciesRoutes.get('/list/:currency', cryptocurrencyInValidator, cryptoCurrenciesController.list);
	}

}

const { cryptoCurrenciesRoutes } = new CryptoCurrenciesRoutes();

export default cryptoCurrenciesRoutes;