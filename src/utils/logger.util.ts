import { Logger } from 'winston';
const { createLogger, format, transports } = require('winston');

class LoggerUtil {
    loggerApp: Logger;
    myFormat: any;
    constructor() {

        this.myFormat = format.printf((message: any) => {
            return `${JSON.stringify(message)}`;

        });

        this.loggerApp = new createLogger({
            format: format.simple(),
            transports: [
                new transports.Console({
                    level: 'info',
                    handleExceptions: true,
                    json: false,
                    colorize: true
                })
            ],
            exitOnError: false
        });
    }
    public printStartMethod(className: string, method: string, ...params: any) {
        this.loggerApp.log('info', `${className} Inicio de método ${method} con paramétros ${JSON.stringify(params)}`);
    }
    // Definición de métodos para realizar sobre carga
    public printEndMethod(className: string, method: string): void;
    public printEndMethod(className: string, method: string, params: any): void;

    public printEndMethod(className: string, method: string, params?: any): void {
        if (params instanceof Array) {
            this.loggerApp.log('info', `${className} Terminó método ${method} con paramétros ${params.length}`);
        } else {
            this.loggerApp.log('info', `${className} Terminó método ${method} `);
        }
    }

    // Definición de métodos para realizar sobre carga
    public imprimirErrormethod(className: string, method: string): void;
    public imprimirErrormethod(className: string, method: string, error: string): void;

    public imprimirErrormethod(className: string, method: string, error?: string): void {
        if (error) {
            this.loggerApp.error(`${className} Terminó método ${method} con error: ${error}`);
        }else{
            this.loggerApp.error(`${className} Terminó método ${method} con error`);
        }

    }

    public error(error: Error): void {
        console.error(error);
    }

    public simple(message: any) {
        this.loggerApp.info(`${message}`);

    }

}



const logComponent = new LoggerUtil();

export default logComponent;
