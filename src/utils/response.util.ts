export interface ResponseModel {
    code: number,
    status: boolean,
    data?: any,
    error?: any,
    message?: string,
}


export default class ResponseUtil {
    static code = 200;
    static status = true;
    data?: any;
    error?: any;
    message?: string;

    public static successfulOnlyMessage(message: string): ResponseModel {
        return {
            status: this.status,
            code: this.code,
            message
        } as ResponseModel;
    }

    public static successfulOnlyMessageAndCode(message: string, code: number): ResponseModel {
        return {
            status: this.status,
            code,
            message
        } as ResponseModel;
    }

    public static failedWithMessageAndCode(message: string, code: number): ResponseModel {
        return {
            status: this.status,
            code,
            message
        } as ResponseModel;
    }

    public static successfulWithData(data: any): ResponseModel {
        return {
            status: this.status,
            code: this.code,
            data
        } as ResponseModel;
    }

    public static error(error: any): ResponseModel {
        return {
            error
        } as ResponseModel;
    }
}