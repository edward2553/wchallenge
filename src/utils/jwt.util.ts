import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { config } from '../config/config';
import JWTValidationError from '../errors/Error';

interface IPayload {
	username: string;
	iat: number;
	exp: number;
}

const { JWT } = config;

export const createToken = (username: string) => {
	return jwt.sign({ username }, JWT.jwt_secret, {
		expiresIn: JWT.jwt_max_age
	});
};

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

	try {
		if (!req.headers || !req.headers['authorization']) {
			res.statusCode = 401;
			res.json({ error: 'El Token es requerido' });
		} else {

			let token = req.headers.authorization.split(' ')[1];

			const { username } = jwt.verify(token, JWT.jwt_secret) as IPayload;
			res.locals.username = username;
			next();
		}

	} catch (error) {
		throw error;
	}
};

export const verifyUser = (check = 'body') => (req: Request, res: Response, next: NextFunction) => {
	if (res.locals.username == req[check as keyof Request].username) {
		next();
	} else {
		res.statusCode = 401;
		res.json({ error: 'Usted no está autorizado para realizar esta acción.' });
	}

}



