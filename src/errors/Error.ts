export class ErrorHandler extends Error {
  data = {};

  constructor(message: any) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

class JWTValidationError extends ErrorHandler {
  constructor(code: number, message: string) {
    super(message);
    this.data = {
      code, message
    }

  }
}

export class ServiceError extends ErrorHandler {
  constructor(code: number, message: string) {
    super(message);
    this.data = {
      code, message
    }

  }
}

export default JWTValidationError;
