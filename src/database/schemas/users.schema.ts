import bcrypt from 'bcryptjs';
import { Schema } from "mongoose";
import { CurrencyEnum } from '../../enum/currency.enum';
import { ServiceError } from '../../errors/Error';
import { ICryptoCurrency } from '../../models/cryptocurrency.model';
import { IUser, IUserModel } from '../../models/users.model';
import mongoose from '../database';

const Users = new Schema<IUser>({
	name: { type: String, required: [true, 'El campo name es requerido'] },
	lastname: { type: String, required: [true, 'El campo lastname es requerido'] },
	username: { type: String, required: [true, 'El campo usernamename es requerido'] },
	password: { type: String, required: [true, 'El campo password es requerido'], minlength: [8, 'La contraseña debe tener al menos 8 caracteres'] },
	currency: { type: String, required: [true, 'El campo currency es requerido'], enum: Object.values(CurrencyEnum), maxlength: [3, 'La longitud máxima es de 3 caracteres'] },
	cryptocurrencies: { type: [String] },
	createAt: { type: Date, default: Date.now }

}, { versionKey: false });

Users.statics.login = async function (username, password): Promise<IUser> {
	const user = await this.findOne({ username }) as IUser;
	if (user) {
		const auth = await bcrypt.compare(password, user.password);
		if (auth) {
			return user;
		}
		throw new ServiceError(500, 'La contraseña es incorrecta');
	};
	throw new ServiceError(500, 'El usuario no existe');
};

Users.statics.addCryptocurrency = async function (username: string, cryptourrency: string): Promise<void> {

	try {
		const currentUser = await this.findOne({ username: username }) as IUser;
		if (currentUser) {

			let currencyIndex = currentUser.cryptocurrencies.findIndex(crypto => crypto == cryptourrency);
			if (currencyIndex === -1) {
				currentUser.cryptocurrencies.push(cryptourrency);
				await this.updateOne({ username }, currentUser);
			} else {
				throw new Error('La cryptomoneda ya existe');
			}
		} else {
			throw new ServiceError(404,'El usuario no existe');
		}

	} catch (error) {
		throw error;
	}

}



Users.pre<IUser>('save', async function (next) {
	const salt = await bcrypt.genSalt();
	this.password = await bcrypt.hash(this.password, salt);
	next();
});

const UserSchema = mongoose.model<IUser, IUserModel>('wc_users', Users);

export default UserSchema;


