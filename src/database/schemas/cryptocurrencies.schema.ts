/**
	symbol: string,
	price: number,
	name: string,
	image: string,
	last_updated: Date */

import { Schema } from "mongoose";
import { ICryptoCurrency } from "../../models/cryptocurrency.model";
import mongoose from 'mongoose';


const CryptoCurrency = new Schema<ICryptoCurrency>({
	symbol: { type: String, required: [true, 'El campo symbol es requerido'] },
	current_price: { type: Number, required: [true, 'El campo current_price es requerido'] },
	name: { type: String, required: [true, 'El campo name es requerido'] },
	image: { type: String, required: [true, 'El campo image es requerido'], },
	last_updated: { type: Date, required: [true, 'El campo last_updated es requerido'] }

}, { versionKey: false });

const CryptoCurrencySchema = mongoose.model<ICryptoCurrency>('wc_cryptocurrency', CryptoCurrency);

export default CryptoCurrencySchema;