import mongoose from 'mongoose';
import { config } from '../config/config';
import logComponent from '../utils/logger.util';

const URL = config.url;

export const init = async () => {

  const mongooseOpts = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  };

  try {
    await mongoose.connect(URL, mongooseOpts);
    logComponent.simple('DB conected');

  } catch (error) {
    console.log(error);
  }
}

export default mongoose;
