import faker from 'faker';
import { ServiceError } from '../errors/Error';
import { IUser } from '../models/users.model';
import UsersService from "../services/users.service";
import ResponseUtil from '../utils/response.util';
import * as mongoosetest from './mocks/mongoose-test';



beforeAll(async () => await mongoosetest.connect());

afterAll(async () => await mongoosetest.closeDatabase());


describe('UserService', () => {


	it('Probando el metodo create cuando ok', async () => {
		// Arrange
		let userData = {
			username: faker.internet.userName(),
			password: faker.internet.password(8),
			name: faker.name.firstName(),
			lastname: faker.name.lastName(),
			currency: 'USD'
		} as IUser;

		const expected = ResponseUtil.successfulOnlyMessage('Usuario creado correctamente');

		// Act
		const result = await UsersService.create(userData);

		// Assert
		expect(result).toEqual(expected);
		expect(result.status).toBeTruthy();

	})

	it('Probando el metodo create cuando usuario existe entonces lanza excepcion', async () => {

		let userData = await initData();

		await expect(UsersService.create(userData)).rejects.toThrow(ServiceError);

	})

	it('Probando el metodo login cuando usuario existe entonces retorna token', async () => {

		let userData = await initData();

		const result = await UsersService.login(userData);


		expect(result).not.toBeNull();
		expect(result.data.username).toEqual(userData.username);

	})

	it('Probando el metodo login cuando usuario no existe entonces lanza excepcion', async () => {

		const userData = await initData();

		let userTest = {
			username: userData.username,
			password: faker.internet.password(9),
		} as IUser;

		await expect(UsersService.login(userTest)).rejects.toThrow(ServiceError);

	})

	it('Probando el metodo addCryptocurrency cuando usuario no existe entonces lanza excepcion', async () => {

		const userData = await initData();

		let userTest = {
			username: faker.internet.userName(),
			password: faker.internet.password(9),
		} as IUser;

		await expect(UsersService.addCryptocurrency(userTest.username, 'bitcoin')).rejects.toThrow(ServiceError);

	})

	it('Probando el metodo addCryptocurrency cuando usuario existe entonces actualiza correctamente', async () => {
		// Arrange
		const userData = await initData();
		const expected = ResponseUtil.successfulOnlyMessage('Se ha actualizado correctamente');

		// Act
		const result = await UsersService.addCryptocurrency(userData.username, 'bitcoin');

		// Assert
		expect(result).not.toBeNull();
		expect(result).toEqual(expected);

	})

	it('Probando el metodo addCryptocurrency cuando existe cryptomoneda entonces lanza excepcion', async () => {
		// Arrange
		const userData = await initDataUserWithCryptocurrencies();

		// Act & Assert
		await expect(UsersService.addCryptocurrency(userData.username, 'bitcoin')).rejects.toThrow(Error);

	})

	it('Probando el metodo listCryptocurrenciesByUser cuando existe usuario entonces retorna la lista', async () => {
		// Arrange
		const userData = await initDataUserWithCryptocurrencies();


		// Act
		const result = await UsersService.listCryptocurrenciesByUser(userData.username, 25, 'ASC');

		// Assert
		expect(result).not.toBeNull();
		expect(result.data).toHaveLength(2);

	})

	it('Probando el metodo listCryptocurrenciesByUser cuando existe usuario y orden desc entonces retorna la lista', async () => {
		// Arrange
		const userData = await initDataUserWithCryptocurrencies();


		// Act
		const result = await UsersService.listCryptocurrenciesByUser(userData.username, 25, 'DESC');

		// Assert
		expect(result).not.toBeNull();
		expect(result.data).toHaveLength(2);

	})

	it('Probando el metodo listCryptocurrenciesByUser cuando tiene 2 cryptocurrency pero sizeList es 1 entonces retorna la lista de 1', async () => {
		// Arrange
		const userData = await initDataUserWithCryptocurrencies();


		// Act
		const result = await UsersService.listCryptocurrenciesByUser(userData.username, 1, 'DESC');

		// Assert
		expect(result).not.toBeNull();
		expect(result.data).toHaveLength(1);

	})

	it('Probando el metodo listCryptocurrenciesByUser y ocurre error entonces lanza excepcion', async () => {
		// Arrange
		await initDataUserWithCryptocurrencies();

		// Act & Assert
		await expect(UsersService.listCryptocurrenciesByUser(faker.internet.userName(), -1, 'DESC')).rejects.toThrow(Error);

	})
})

const initData = async () => {
	let userData = {
		username: faker.internet.userName(),
		password: faker.internet.password(8),
		name: faker.name.firstName(),
		lastname: faker.name.lastName(),
		currency: 'USD'
	} as IUser;
	userData.cryptocurrencies = [];
	await UsersService.create(userData);

	return userData;
}

const initDataUserWithCryptocurrencies = async () => {
	let userData = {
		username: faker.internet.userName(),
		password: faker.internet.password(8),
		name: faker.name.firstName(),
		lastname: faker.name.lastName(),
		currency: 'USD',
		cryptocurrencies: ['bitcoin', 'ethereum']
	} as IUser;
	await UsersService.create(userData);

	return userData;
}
