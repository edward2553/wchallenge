import cryptoCurrenciesService from '../services/cryptocurrencies.service';
describe('UserService', () => {

	it('Probando el metodo list cuando ok', async () => {

		// Arrange
		const currency = 'USD';

		// Act
		const result = await cryptoCurrenciesService.list(currency);

		// Assert
		expect(result).not.toBeNull();
		expect(result.data.length).toBeGreaterThan(0);
		expect(result.status).toBeTruthy();

	})

	it('Probando el metodo list cuando currency no valido entonces lanza excepcion', async () => {

		// Arrange
		const currency = 'COP';

		// Act && Assert
		await expect(cryptoCurrenciesService.list(currency)).rejects.toThrowError();

	})

})