export const cryptocurrencies = [
	{
		id: "bitcoin",
		symbol: "btc",
		current_price: 31002,
		name: "Bitcoin",
		image: "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
		last_updated: "2021-01-28T02:50:36.794Z"
	},
	{
		id: "ethereum",
		symbol: "eth",
		current_price: 1270.16,
		name: "Ethereum",
		image: "https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880",
		last_updated: "2021-01-28T02:52:01.201Z"
	},
	{
		id: "tether",
		symbol: "usdt",
		current_price: 1,
		name: "Tether",
		image: "https://assets.coingecko.com/coins/images/325/large/Tether-logo.png?1598003707",
		last_updated: "2021-01-28T02:09:46.203Z"
	},
	{
		id: "polkadot",
		symbol: "dot",
		current_price: 15.93,
		name: "Polkadot",
		image: "https://assets.coingecko.com/coins/images/12171/large/aJGBjJFU_400x400.jpg?1597804776",
		last_updated: "2021-01-28T02:51:52.820Z"
	},
];

