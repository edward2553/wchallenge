import ResponseUtil from '../../utils/response.util';
export const usersMock = [
	{
		_id: "6010cace87194353f8f7ae7e",
		cryptocurrencies: ["bitcoin", "polkadot", "ethereum", "tether", "ripple", "cardano", "chainlink", "litecoin"],
		username: "harwside",
		password: "$2a$10$ON0P28tPgtfkYE8VHTn12e.hGHeyRMkzvq.6LwT1/xR1yWRYz/FCK",
		name: "Harwin",
		lastname: "Silva",
		currency: "USD",
		createAt: "2021-01-27T02:07:10.324Z"
	},
	{
		_id: "600cec263257890a407dfe99",
		cryptocurrencies: ["bitcoin", "ethereum"],
		username: "pepiperez",
		password: "$2a$10$i/IuSrtIklB9VWooLOda4u1FzQMORZ6dRx1VIvj62HmJTfZV24BCm",
		name: "Pedro",
		lastname: "Pérez",
		currency: "USD",
		createAt: "2021-01-24T03:40:22.414Z"
	}
];

export const cryptocurrenciesByUserMock = [
	{
		id: "bitcoin",
		symbol: "btc",
		name: "Bitcoin",
		image: "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
		last_updated: "2021-01-28T02:57:53.651Z",
		current_price_ARS: 2703592,
		current_price_EUR: 25668,
		current_price_USD: 31036
	},
	{
		id: "ethereum",
		symbol: "eth",
		name: "Ethereum",
		image: "https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880",
		last_updated: "2021-01-28T02:57:54.910Z",
		current_price_ARS: 110622,
		current_price_EUR: 1050.25,
		current_price_USD: 1269.87
	},
	{
		id: "litecoin",
		symbol: "ltc",
		name: "Litecoin",
		image: "https://assets.coingecko.com/coins/images/2/large/litecoin.png?1547033580",
		last_updated: "2021-01-28T02:57:57.701Z",
		current_price_ARS: 10889.89,
		current_price_EUR: 103.39,
		current_price_USD: 125.01
	},
	{
		id: "chainlink",
		symbol: "link",
		name: "Chainlink",
		image: "https://assets.coingecko.com/coins/images/877/large/chainlink-new-logo.png?1547034700",
		last_updated: "2021-01-28T02:57:08.474Z",
		current_price_ARS: 1903.14,
		current_price_EUR: 18.07,
		current_price_USD: 21.85
	},
	{
		id: "polkadot",
		symbol: "dot",
		name: "Polkadot",
		image: "https://assets.coingecko.com/coins/images/12171/large/aJGBjJFU_400x400.jpg?1597804776",
		last_updated: "2021-01-28T02:57:31.548Z",
		current_price_ARS: 1392.23,
		current_price_EUR: 13.22,
		current_price_USD: 15.98
	},
	{
		id: "tether",
		symbol: "usdt",
		name: "Tether",
		image: "https://assets.coingecko.com/coins/images/325/large/Tether-logo.png?1598003707",
		last_updated: "2021-01-28T02:09:46.203Z",
		current_price_ARS: 87.28,
		current_price_EUR: 0.827968,
		current_price_USD: 1
	},
	{
		id: "cardano",
		symbol: "ada",
		name: "Cardano",
		image: "https://assets.coingecko.com/coins/images/975/large/cardano.png?1547034860",
		last_updated: "2021-01-28T02:57:40.836Z",
		current_price_ARS: 27.83,
		current_price_EUR: 0.264247,
		current_price_USD: 0.319505
	},
	{
		id: "ripple",
		symbol: "xrp",
		name: "XRP",
		image: "https://assets.coingecko.com/coins/images/44/large/xrp-symbol-white-128.png?1605778731",
		last_updated: "2021-01-28T02:57:49.067Z",
		current_price_ARS: 22.09,
		current_price_EUR: 0.209739,
		current_price_USD: 0.253599
	}
];

