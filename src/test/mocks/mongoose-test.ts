
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

const mongod = new MongoMemoryServer();


export const connect = async () => {
	await mongod.start();

	const uri = await mongod.getUri();

	const mongooseOpts = {
		useNewUrlParser: true,
		useCreateIndex: true,
		useUnifiedTopology: true,
		useFindAndModify: false
	};

	// mongoose.connect(uri, mongooseOpts).then((db: any) => logComponent.simple('Db connected'))
	// 	.catch((err: any) => console.error(err.message));

	await mongoose.connect(uri, mongooseOpts);
}

export const closeDatabase = async () => {
	await mongoose.connection.dropDatabase();
	await mongoose.connection.close();
	await mongod.stop();
}

export const clearDatabase = async () => {
	const collections = mongoose.connection.collections;

	for (const key in collections) {
		const collection = collections[key];
		await collection.drop();
	}
}