import { NextFunction, Request, Response } from 'express';
import JWTValidationError from '../errors/Error';
import { createToken, verifyToken } from '../utils/jwt.util';

describe('jwt util test', () => {
	let mockRequest: Partial<Request>;
	let mockResponse: Partial<Response>;
	let nextFunction: NextFunction = jest.fn();

	beforeEach(() => {
		mockRequest = {};
		mockResponse = {
			json: jest.fn(),
			locals: {}
		};
	});

	it('Probando metodo verifyToken cuando los headers estan vacios', async () => {
		const expectedResponse = {
			"error": "El Token es requerido"
		};

		verifyToken(mockRequest as Request, mockResponse as Response, nextFunction)

		expect(mockResponse.json).toBeCalledWith(expectedResponse);

	});

	it('Probando metodo verifyToken cuando no envía authorization en los headers', async () => {
		const expectedResponse = {
			"error": "El Token es requerido"
		};
		mockRequest = {
			headers: {
			}
		}
		verifyToken(mockRequest as Request, mockResponse as Response, nextFunction);

		expect(mockResponse.json).toBeCalledWith(expectedResponse);
	});

	it('Probando metodo verifyToken cuando envía authorization en los headers entonces llama next', async () => {
		mockRequest = {
			headers: {
				'authorization': `Bearer ${createToken('test')}`
			}
		}
		verifyToken(mockRequest as Request, mockResponse as Response, nextFunction);

		expect(nextFunction).toBeCalledTimes(1);
	});
});