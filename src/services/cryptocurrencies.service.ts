import logComponent from '../utils/logger.util';
import coingeckoWSConsumer from '../consumers/coingecko.ws.consumer';
import { config } from '../config/config';
import ResponseUtil from '../utils/response.util';

const props = config.COINGECKO.COINGECKO_ATTRS;

class CryptoCurrenciesService {

	constructor() { }

	public async list(currency: string) {

		try {
			const result = await coingeckoWSConsumer.listCoins(currency) as any[];
			let filterResult = result.map((element: any) => props.reduce((actual, prop) => ({ ...actual, ...(element.hasOwnProperty(prop) && { [prop]: element[prop] }) }), {}));
			return ResponseUtil.successfulWithData(filterResult);
		} catch (error) {
			throw error;
		}
	}

}

const cryptoCurrenciesService = new CryptoCurrenciesService();

export default cryptoCurrenciesService;