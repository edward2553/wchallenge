import UsersSchema from '../database/schemas/users.schema';
import { ServiceError } from '../errors/Error';
import { ICryptoCurrency } from '../models/cryptocurrency.model';
import { IUser } from '../models/users.model';
import { createToken } from '../utils/jwt.util';
import ResponseUtil from '../utils/response.util';
import UserSchema from '../database/schemas/users.schema';
import coingeckoWSConsumer from '../consumers/coingecko.ws.consumer';
import { CurrencyEnum } from '../enum/currency.enum';
import { config } from '../config/config';
const props = config.COINGECKO.COINGECKO_ATTRS;


class UserService {

	constructor() { }

	async create(userData: IUser) {

		try {

			const exist = await UsersSchema.findOne({ username: userData.username });

			if (exist) {
				throw new ServiceError(601, `El usuario ${userData.username} ya existe`);
			}
			await UsersSchema.create(userData);
			return ResponseUtil.successfulOnlyMessage('Usuario creado correctamente');
		} catch (error) {
			throw error;
		}

	}

	async login(userData: IUser) {

		try {

			let user = await UsersSchema.login(userData.username, userData.password);

			return ResponseUtil.successfulWithData({ username: user.username, token: createToken(user.username) });
		} catch (error) {
			throw error;
		}

	}


	async addCryptocurrency(username: string, cryptocurrency: string) {

		try {
			await UsersSchema.addCryptocurrency(username, cryptocurrency);
			return ResponseUtil.successfulOnlyMessage('Se ha actualizado correctamente');

		} catch (error) {
			throw error;
		}

	}

	async listCryptocurrenciesByUser(username: string, sizeList: number, order: string) {

		try {
			const user = await UserSchema.findOne({ username }) as IUser;

			let cryptocurrenciesWithCurrency: any[] = await this.getCryptocurrenciesByUser(user.cryptocurrencies);

			cryptocurrenciesWithCurrency = cryptocurrenciesWithCurrency.sort(this.order(order, user.currency)).slice(0, sizeList);

			return ResponseUtil.successfulWithData(cryptocurrenciesWithCurrency);

		} catch (error) {
			throw error;
		}

	}


	private async getCryptocurrenciesByUser(ccIds: string[]) {
		let cryptocurrencies: any[] = [];

		for (const currency of Object.keys(CurrencyEnum)) {

			const cryptoCurrenciesByCurrency = await coingeckoWSConsumer.getCryptocurrenciesByUser(currency, ccIds) as any[];

			let filterResult = this.filterFields(cryptoCurrenciesByCurrency);

			filterResult.forEach((data: any, i: number) => {
				const index = cryptocurrencies.findIndex((cryptocurrency: any) => cryptocurrency.id == data.id);
				if (index === -1) {
					cryptocurrencies.push(data);
				}
				this.addPropertyByCurrency(cryptocurrencies, i, currency, data);
				delete cryptocurrencies[i].current_price;
			});

		}
		return cryptocurrencies;
	}

	private addPropertyByCurrency(cryptocurrencies: any[], i: number, currency: string, data: any) {
		Object.defineProperty(cryptocurrencies[i], 'current_price_' + currency, {
			value: data.current_price,
			writable: true,
			enumerable: true,
			configurable: true
		});
	}

	private filterFields(cryptoCurrenciesByCurrency: any[]) {
		return cryptoCurrenciesByCurrency.map((element: any) => {
			return props.reduce((actual, prop) => ({ ...actual, ...(element.hasOwnProperty(prop) && { [prop]: element[prop] }) }), {});
		});
	}

	private order(order: string, currency: string): ((a: any, b: any) => number) | undefined {
		if (order === 'ASC') {
			return (ccA, ccB) => ccA['current_price_' + currency] - ccB['current_price_' + currency];
		} else if (order === 'DESC') {
			return (ccA, ccB) => ccB['current_price_' + currency] - ccA['current_price_' + currency];
		}
	}
}

export default new UserService();