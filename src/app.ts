import { urlencoded } from 'body-parser';
import cors from 'cors';
import express, { Application } from 'express';
import morgan from 'morgan';
import { config } from "./config/config";
import { errorHandler, logErrors } from './middlewares/errorHandlers';
import IndexRouter from './routes/index.routes';


import * as db from './database/database';
import logComponent from './utils/logger.util';
class App {
	public app: Application;

	constructor() {

		this.app = express();
		this.config();
		this.routes();

	}

	config(): void {

		this.app.set('port', config.port);
		this.app.use(morgan('dev'));
		this.app.use(express.json());
		this.app.use(cors());
		this.app.use(urlencoded({ extended: true }));
		db.init();

	}

	routes(): void {
		this.app.use('/api', IndexRouter);

		this.app.use(logErrors);
		this.app.use(errorHandler);
		this.app.use((req, res) => {
			res.status(404).json({
				status: 'not found',
				code: 404,
				error: 'El recurso que esta consultando no está disponible'
			});

		});


	}

	start(): void {
		this.app.listen(this.app.get('port'), () => {
			logComponent.simple(`Server on port ${this.app.get('port')}`);
		});

	}

}

export default new App();
