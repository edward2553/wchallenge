import { NextFunction, Request, Response } from 'express';
import coingeckoWSConsumer from '../consumers/coingecko.ws.consumer';
import ResponseUtil from '../utils/response.util';
import { config } from '../config/config';
import logComponent from '../utils/logger.util';
import cryptoCurrenciesService from '../services/cryptocurrencies.service';
const props = config.COINGECKO.COINGECKO_ATTRS;

class CryptoCurrenciesController {

	public async list(req: Request, res: Response, next: NextFunction) {

		logComponent.printStartMethod(CryptoCurrenciesController.name, 'list', req.params);

		try {
			const { currency } = req.params;
			const result = await cryptoCurrenciesService.list(currency);
			res.json(result);
		} catch (error) {
			next(error);
		}
		logComponent.printEndMethod(CryptoCurrenciesController.name, 'list');

	}


}

export const cryptoCurrenciesController = new CryptoCurrenciesController();
