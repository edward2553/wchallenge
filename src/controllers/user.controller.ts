import { NextFunction, Request, Response } from 'express';
import usersService from "../services/users.service";
import logComponent from "../utils/logger.util";


class UserController {

	constructor() {
	}

	async login(req: Request, res: Response, next: NextFunction) {
		logComponent.printStartMethod(UserController.name, 'login', req.body);
		try {
			const result = await usersService.login(req.body);
			res.json(result);
		} catch (error) {
			next(error);
		}
		logComponent.printEndMethod(UserController.name, 'login');
	}

	async create(req: Request, res: Response, next: NextFunction) {
		logComponent.printStartMethod(UserController.name, 'createUser', req.body);
		try {
			const result = await usersService.create(req.body);
			res.json(result);
		} catch (error) {
			next(error);
		}
		logComponent.printEndMethod(UserController.name, 'createUser');
	}

	async addCryptocurrency(req: Request, res: Response, next: NextFunction) {
		logComponent.printStartMethod(UserController.name, 'addCryptocurrency', req.body);

		try {
			const { username, cryptocurrency }: { username: string, cryptocurrency: string } = req.body;

			const result = await usersService.addCryptocurrency(username, cryptocurrency);

			res.json(result);
		} catch (error) {
			next(error);
		}
		logComponent.printEndMethod(UserController.name, 'addCryptocurrency');

	}

	async listCryptocurrenciesByUser(req: Request, res: Response, next: NextFunction) {
		logComponent.printStartMethod(UserController.name, 'listCryptocurrenciesByUser', req.query);

		try {
			const username = req.query.username as string;
			const sizeList = parseInt(req.query.sizeList as string) || 25;
			const order = req.query.order as string || 'ASC';

			const result = await usersService.listCryptocurrenciesByUser(username, sizeList, order);

			res.json(result);
		} catch (error) {
			next(error);
		}
		logComponent.printEndMethod(UserController.name, 'listCryptocurrenciesByUser');

	}
}

const userController = new UserController();

export default userController;