import { Request, Response } from 'express';

class IndexController {

	public index(req: Request, res: Response) {

		res.status(200).json(
			{
				status: true,
				code: 200,
				message: 'API v1'
			});

	}
}

export const indexController = new IndexController();
