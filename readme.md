# Cryptocurrencies Monitor


## Instrucciones

### Instalar dependencias
>`npm install`

### Ejecutar pruebas unitarias
Las pruebas unitarias están construidas con jest y para las consultas a base de datos hace uso de base en memoria con `mongodb-memory-server`
>`npm run test`

### Para ejecutar localmente
>`npm run start`

## Dependencias
Para la construcción del proyecto se mencionan a continuación algunas de las dependencias más relevantes.
- Express.js
- axios
- mongoose
- Typescript


#### [Documentacion](https://documenter.getpostman.com/view/4109565/TW6xn7ix)
